var base_url = window.location.origin+'/roma/';

var url = window.location.href;
var swLocation = '/roma/sw.js';

var swReg;

if ( navigator.serviceWorker ) {
    window.addEventListener('load', function() {

        navigator.serviceWorker.register( swLocation ).then( function(reg){

            swReg = reg;
            //swReg.pushManager.getSubscription().then();
            swReg.pushManager.getSubscription();
        });

    });
}

// Inclusió de jsClosest com a funció nativa de javascript
(function (ElementProto) {
	if (typeof ElementProto.matches !== 'function') {
		ElementProto.matches = ElementProto.msMatchesSelector || ElementProto.mozMatchesSelector || ElementProto.webkitMatchesSelector || function matches(selector) {
			var element = this;
			var elements = (element.document || element.ownerDocument).querySelectorAll(selector);
			var index = 0;

			while (elements[index] && elements[index] !== element) {
				++index;
			}

			return Boolean(elements[index]);
		};
	}

	if (typeof ElementProto.jsClosest !== 'function') {
		ElementProto.jsClosest = function jsClosest(selector) {
			var element = this;

			while (element && element.nodeType === 1) {
				if (element.matches(selector)) {
					return element;
				}

				element = element.parentNode;
			}

			return null;
		};
	}
})(window.Element.prototype);

//toggleClass native
function toggleClass(el, cls){
    if(el.classList.contains(cls)){
        el.classList.remove(cls);
    }else{
        el.classList.add(cls);
    }
}

$(document).ready(function(){
    // iOS web app full screen hacks.
    if(window.navigator.standalone == true) {
            // make all link remain in web app mode.
            $('a').click(function() {
                    window.location = $(this).attr('href');
                    $('#navbar').addClass('topbar');
                    return false;
            });
    }
});


function getStorage(){
    var json = (localStorage.getItem('Enigma')===null) ? {} : JSON.parse(localStorage.getItem('Enigma'));
    return json;
}

function setStorage(order){
    localStorage.setItem('Enigma',JSON.stringify(order));
}

function numberToString(val){
    return JSON.stringify(JSON.parse(val))
}

function stringToBoolean(val){
    if(val === "true" || val === "false"){
        return JSON.parse(val);
    }else{
        return false;
    }
}

function compareString(el,resp){
    var userResp = el.toLowerCase().trim(),
        realResp = resp.toLowerCase().trim();

    return (userResp === realResp) ? true : false;  

}

function compareArrayStrings(el,arr){
    var aux = false;
    arr.forEach(function(resp){
        if(compareString(el,resp)){
            aux = true;
        }
    });
    return aux;
}

function compareNumber(el,resp){
    var userResp = parseInt(el),
        realResp = parseInt(resp);

    return (userResp === realResp) ? true : false;
}

function compareTwoArrays(arr1, arr2){
    var aux = (arr1.length == arr2.length) ? true : false;

    if(aux === true){
        arr1.forEach(function(el,i){
            if(el !== arr2[i]){ aux = false}
        });
    }

    return aux;

}

function randomArray(total){
    var aux = [];

    for(i = 0; i<total; i++){
        aux.push(i);
    }
    
    aux = aux.sort(function() {return Math.random() - 0.5});

    return aux;

}

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
}
  
function getDistanceFromLatLonInMeters(lat1,lon1,lat2,lon2) {
    var R = 6371000; // Radius of the earth in meters
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
}
  
function deg2rad(deg) {
    return deg * (Math.PI/180)
}

//Start Fake
(function(){
    var el = document.querySelector('.m-form--start-game');
    
    if(el!==null){
        var bUbication = document.querySelector('.a-button--ubicacion'),
            bStart = document.querySelector('.a-button--start'),
            sUbication = document.querySelector('.ubication-start .small');

        bUbication.addEventListener('click',changeButtons,false);

        function changeButtons(){
            bUbication.classList.add('correct');
            bStart.removeAttribute("disabled");
            sUbication.innerText = 'Circo Máximo, Via del Circo Massimo, Roma, Italia';
            bStart.addEventListener('click',progress,false);
        }

        function progress(){
            location.href=base_url+'questions/question.html';
        }
    }   
})();

//Button Back
(function(){
    var el = document.querySelector('.a-button--back');

    if(el !== null){
        el.addEventListener('click',goBack,false);
    }

    function goBack() {
        var el2 = $('.p-init-register-form .step.step2');
        if(el2.length && el2.hasClass('active')){
            $('.step.step1').addClass('active');
            $('.step.step2').removeClass('active');
        }else{
            window.history.back();
        }
    }
})();

//Mecanica de buttons Game module
(function(){
    var el = document.querySelector('.o-game-module');

    if(el !== null){
        el.querySelector('.a-button--more').addEventListener('click',activeHeader,false);
        el.querySelector('.a-button--answer').addEventListener('click',activeAnswer,false);
    };

    function activeHeader(){
        var father = this.jsClosest('.ogm-header');
        toggleClass(father, 'active');
    }

    function activeAnswer(){
        var father = this.jsClosest('.o-game-module');
        toggleClass(father, 'active');
    }
})();

// Inicialització del joc.
(function(){
    var el = document.querySelector('.o-game-module'),
        response = 
        {
            1: {
                answers : [['Ben-hur','Benhur']],
                pistas: ["Protagonizada por Charlton Heston", "Ganó 11 Oscars"],
                games : 'text',
                totalGames: 1,
                current: 1,
                step: 1,
                totalSteps: 13,
                idProduct: 1,
                place: 'CIRCO MASSIMO',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_CircoMassimo.jpg',
                story: 'El juego empieza en el Circo Maximo, el estadio deportivo más grande jamás construido con capacidad para 300mil espectadores. Allí se celebraban diferentes competiciones como las carreras de caballos o diferentes espectáculos.',
                question: 'Aunque apenas quedan restos del Circo Máximo ¿puedes imaginarte participando en una carrera de carros? En una película rodada en 1959  aparece una legendaria carrera que se convirtió en una de las escenas más míticas del cine.',
                extra: '10mil extras formaron parte de la pelicula. Para rodar la carrera de carros, se construyó un estadio romano real, el escenario más grande de la história.',
                currentStep: 1,
                nextStep: 2
            },
            2: {
                answers : [['Gladiator']],
                pistas: ["Protagonizada por Russell Crowe", "El titulo de la pel·lícula (en inglés)  es el nombre que recibían los luchadores que entretenian al público con sus duras batallas.","Ganó 5 premios Óscar"],
                games : 'text',
                totalGames: 1,
                current: 1,
                step: 2,
                totalSteps: 13,
                idProduct: 1,
                place: 'PROXIMO DESTINO',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_Coliseum_2.jpg',
                story: 'Vuestro próximo destino es el principal símbolo de Roma. Allí, en este anfiteatro construido en el siglo I, más de 50.000 personas podían disfrutar de sus espectáculos preferidos, como las muestras de animales exóticos, ejecuciones de prisioneros, recreaciones de batallas o las peleas de gladiadores.',
                question: 'Aquí se ambientó una película que hizo resurgir el interés por las películas de cine épico y por la antigua Roma. La película trata la vida de Máximo, general del ejército del Imperio romano que es forzado a convertirse en esclavo, traicionado por el hijo del emperador. Máximo triunfa en la arena del anfiteatro.',
                extra: 'Se contruyó una replica del Coliseo en Malta de unos 16m de altura. El resto del Anfiteatro se añadió digitalmente.',
                currentStep: 2,
                nextStep: 3
            },
            3: {
                answers : ["",['8','ocho']],
                optionsImg: [base_url+'assets/img/temp/roma-cine/2857.jpg'],
                pistas: ["La fotografía la podéis encontrar en el interior del Colisseo", "Se encuentra en la exposición fija"],
                games : 'puzzleText',
                totalGames: 2,
                current: 1,
                step: 3,
                totalSteps: 13,
                idProduct: 1,
                place: 'COLISEUM',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_Coliseum.jpg',
                story: 'La inmensidad del anfiteatro aclamaba al coronel Máximo en su batalla conra el invicto Tigris de la Galia y la lucha con los leones y tigres que  surgían de los laberintos del subsuelo.',
                question: 'Encajad todas las piezas y averiguar ¿cuántos animales véis?',
                extra: 'Se calcula que entre las peleas de gladiadores murieron unas 400.000 personas y más de 1.000.000 de animales',
                currentStep: 3,
                nextStep: 4
            },
            4: {
                answers : [['A Roma con Amor']],
                pistas: ["Woody Allen vuelve a actuar en una pelicula, algo que no hacia desde Scroop en 2006", "Pelicula con 4 palabras dónde la palabra de la ciudad que nos ocupa es protagonista.", "El amor también es protagonista."],
                games : 'text',
                totalGames: 1,
                current: 1,
                step: 4,
                totalSteps: 13,
                idProduct: 1,
                place: 'PROXIMO DESTINO',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_FontanaDiTrevi.jpg',
                story: 'Si hay un lugar en Roma dónde se han rodado más películas ese es vuestro próximo destino. Su nombre de debe a que la fuente se sitúa en un plaza donde se encuentran 3 calles ( Trie Vie). Cuando lleguéis quizás no os podéis resistir a tirar una moneda. ',
                question: 'Película del 2012 del oscarizado Woody Allen donde relata la vida y curiosidades de 4 parejas con un escenario común: la ciudad de Roma. Las historias tratan sobre el amor y la realización. ',
                extra: 'La pelicula está basada en algunos fragmentos del Decameron de Bocaccio.',
                currentStep: 4,
                nextStep: 5
            },
            5: {
                answers : ['estatua2','estatua5','estatua6','estatua3'],
                options: ['estatua6','estatua2','estatua3','estatua5'],
                optionsImg: [[base_url+'assets/img/temp/roma-cine/estatua6.jpg',
                              base_url+'assets/img/temp/roma-cine/estatua2.jpg',
                              base_url+'assets/img/temp/roma-cine/estatua3.jpg',
                              base_url+'assets/img/temp/roma-cine/estatua5.jpg']],
                pistas: ["La secuencia será siempre de izquierda a derecha"],
                games : 'orderImg',
                totalGames: 1,
                current: 1,
                step: 5,
                totalSteps: 13,
                idProduct: 1,
                place: 'FONTANA DI TREVI',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_FontanaDiTrevi_2.jpg',
                story: 'En esta mítica ubicación se inicia una de las cuatro historias, el flechazo de Michelangelo hacia Hayley mientras ella buscaba la fuente para tirar unas monedas.',
                question: 'En lo alto de la fuente estátuas de cuatro mujeres que representaban que el agua que llega a la fuente era pura y se podía beber.  ¿Sabrías seleccionarlas por orden?',
                extra: 'Todo el dinero que se recoge en la Fontana di Trevi se destina a fines benéficos. Se estima que se recauda más o menos un millor de euros anuales.',
                currentStep: 5,
                nextStep: 6
            },
            6: {
                answers : [['El Talento de Mr. Ripley']],
                pistas: ["Película de 5 palabras", "Las dos palabras principales ya os las hemos dicho en la descripción del enigma","En el título aparece la abreviatura Mr."],
                games : 'text',
                totalGames: 1,
                current: 1,
                step: 6,
                totalSteps: 13,
                idProduct: 1,
                place: 'PROXIMO DESTINO',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_PiazzaDiSpagna_2.jpg',
                story: 'Para poder llegar a al próximo destino deberéis encontrar 135 peldanos que separan la Embajada de España con la iglesia Trinità di Monti.',
                question: 'Por esta plaza pasará Tom Ripley durante su viaje a Italia, enviado por un hombre rico al que ha hecho creer, con su enorme talento para el engaño, que es amigo de su hijo, al que debe convencer para que vuelva a EEUU.',
                extra: 'Basada en la más célebre novela de Patricia Highsmith. Con el título A pleno sol, la película fué llevada al cine en 1960 por René Clement, con Alain Delon en el papel de Ripley. En 1999 se estrenó el remake El talento de Mr. Ripley.',
                currentStep: 6,
                nextStep: 7
            },
            7: {
                answers : [['sol']],
                options: [['estrella', 'luna', 'sol', 'saturno']],
                optionsImg: [[base_url+'assets/img/temp/roma-cine/icono1.png',
                              base_url+'assets/img/temp/roma-cine/icono2.png',
                              base_url+'assets/img/temp/roma-cine/icono3.png',
                              base_url+'assets/img/temp/roma-cine/icono4.png']],
                pistas: ["Es una estrella que nos ilumina durante el dia", "De tres letras"],
                games : 'chooseImg',
                totalGames: 1,
                current: 1,
                step: 7,
                totalSteps: 13,
                idProduct: 1,
                place: 'PIAZZA DI SPAGNA',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_PiazzaDiSpagna.jpg',
                story: 'Delante de la majestuosa esacalinata, Ton recoge con su moto a Marge rodeando la fuente de a Barcazza (dejándola a la izquierda).',
                question: 'En la fuente, un símbolo abastece de agua la fuente en sus dos extremos.',
                extra: 'Cuenta la leyenda que, en una crecida del río Tiber, un barco encalló en este punto y de ahí le viene el nombre y forma de la fuente.',
                currentStep: 7,
                nextStep: 8
            },
            8: {
                answers : [['Come, Reza, Ama', 'Come Reza Ama']],
                pistas: ["Protagonizada por Julia Roberts y Javier Bardem.", "El título de la película esta compuesto por 3 verbos.","C___, R___, A---"],
                games : 'text',
                totalGames: 1,
                current: 1,
                step: 8,
                totalSteps: 13,
                idProduct: 1,
                place: 'PROXIMO DESTINO',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_PiazzaNavona.jpg',
                story: 'Dirigíos a la plaza barroca por excelencia de Roma. Rodeada de callejuelas por las que es ideal perderse. Su principal atracción es la famosa Fuente de los cuatro ríos, diseñada por Bernini.',
                question: 'Roma es uno de los destinos de Liz, que tras varios fracasos sentimentales decide encotrarse a si misma a través de un inolvidable viaje, el cual también le llevará a través de la Índia e Indonesia.',
                extra: 'Adaptación de la novela autobiográfica de Elisabeth Gilbert, dónde nos explica el largo viaje que emprendió para encontrarse a sí misma.',
                currentStep: 8,
                nextStep: 9
            },
            9: {
                answers : [4],
                options: [5],
                optionsImg: { rated: base_url+'assets/img/temp/roma-cine/key.svg', 
                                unrated: base_url+'assets/img/temp/roma-cine/key_unrated.svg'},
                pistas: ["Buscad en la fuente de los cuatro rios"],
                games : 'rating',
                totalGames: 1,
                current: 1,
                step: 9,
                totalSteps: 13,
                idProduct: 1,
                place: 'PIAZZA NAVONA',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_PiazzaNavona_2.jpg',
                story: 'Liz está sentada en este banco comiendo un helado, observando la fuente principal y pensand cuántas llaves necesitará usar para abrir la puerta correcta que le lleve a su autorealización personal.',
                question: '¿Sabríais averiguar cuántas llaves necesitará?',
                extra: 'Las estátuas que componen la fuente de los 4 ríos son alegorías de los 4 principales ríos del mundo: Nilo, Ganges, Danubio y el río de la plata.',
                currentStep: 9,
                nextStep: 10
            },
            10: {
                answers : [['Angeles y demonios', 'Ángeles y demonios']],
                pistas: ["Basada en la novela de Dan Brown","Protagonizada por Tom Hanks","Es la secuela de la película de 2006, El Código da Vinci"],
                games : 'text',
                totalGames: 1,
                current: 1,
                step: 10,
                totalSteps: 13,
                idProduct: 1,
                place: 'PROXIMO DESTINO',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_Castel_SantAngelo.jpg',
                story: 'Muy cerca de la ciudad del Vaticano se encuentra la ubicación de vuestro siguiente reto. Situado a la orilla derecha del río Tiber, este castillo también es conocido como El Mausoleo de Adriano.',
                question: 'El profesor de simbología religiosa Robert Langdom, se ve de pronto sumido en la búsqueda del arma más mortífera de la humanidad, la antimateria,  que los Iluminati han puesto en el Vaticano con el fin de destruir la Iglesia Católica.  Al mismo tiempo intentará rescatar a 4 cardenades que también han secuestrado.',
                extra: 'En Los Ángeles se hicieron réplicas de la Plaza de San Pedro,  Piazza Navona, la Capilla Sixtina, el Pantenon, el castillo de San Ángelo, y los frescos y estátuas de Miguel Ángel i Bernini.  Lo más costoso fué la reproducción que se hizo de la Plaza de Sant Pedro en el aparcamiento de Hollywood park.',
                currentStep: 10,
                nextStep: 11
            },
            11: {
                answers : ["",['Cruz']],
                optionsImg: [base_url+'assets/img/temp/roma-cine/2711.jpg'],
                pistas: [],
                games : 'puzzleText',
                totalGames: 2,
                current: 1,
                step: 11,
                totalSteps: 13,
                idProduct: 1,
                place: "CASTEL SANT'ANGELO",
                coverImg: base_url+'assets/img/temp/roma-cine/bg_Castel_SantAngelo_2.jpg',
                story: 'El profesor descubre que tienen retenidos a los cardenales en el castillo, al que llamaban, iglesia de la iluminación. En si afán por salvar también al Vaticano, se dirige hacia allí, traspasando el puente protegido por 10 ángeles. ',
                question: 'El símbolo que sostiene uno de los ángeles os ayudará a avanzar en vuestra aventura.',
                extra: 'El castillo posee numerosos pasillos y corredores secretos, incluso alguno de ellos, con más de 1km de logitud conectan con el Vaticano.',
                currentStep: 11,
                nextStep: 12
            },
            12: {
                answers : [['Misión Imposible III', 'Mision Imposible III', 'MiIII']],
                pistas: ["Protagonizada por Tom Cruise", "Es la tercera entrega de la saga","En el títula aparecen números romanos"],
                games : 'text',
                totalGames: 1,
                current: 1,
                step: 12,
                totalSteps: 13,
                idProduct: 1,
                place: 'PROXIMO DESTINO',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_CittaDiVaticano_2.jpg',
                story: 'Valientes cinéfilos!!  el último reto lo encontraréis en un país soberano, uno de los seis microestados europeos.  Allí se encuentra la Santa Sede, máxima institución de la Iglésia Católica.',
                question: 'Ethan Hunt es un espía retirado del “Imposible Mission Force” (IMF) que se dedica a entrenar a nuevos reclutas. Sin embargo, es enviado por tercera vez a la acción para localizar al evasivo traficante de armas Owen Davian.',
                extra: 'En Misión: Imposible III hay un total de 16 explosiones.',
                currentStep: 12,
                nextStep: 13
            },
            13: {
                answers : [['1586']],
                pistas: ["MDLXXXVI", "D=500"],
                games : 'text',
                totalGames: 1,
                current: 1,
                step: 13,
                totalSteps: 13,
                idProduct: 1,
                place: 'CITTA DI VATICANO',
                coverImg: base_url+'assets/img/temp/roma-cine/bg_CittaDiVaticano.jpg',
                story: 'Cuando informan al equipo de Ethan Hunt que Owen se enuentra en la que se considera la ciudad más pequeña del mundo, se dirigen hacia allí para secuestrarlo.',
                question: 'En el centro de la plaza de Sant Pedro, con el viento en Ostro, mirando hacia el obelisco, nos indica el año de construcción.',
                extra: 'Fué declarado patrimonio de la Humanidad por la Unesco en 1984.',
                currentStep: 13,
                nextStep: -1
            },
        }
    if(el!==null){
        //setStorage({});
        var store = getStorage(),
            step = (store.currentStep === undefined) ? 1 : store.currentStep;
        
            setGame(response[step]);

    };
})();


/*
 *  SET GAME
 *  Aquesta funció crida els diferents moduls per a cada joc.
 * 
 */

function setGame(response){
    moduleAnswers(response);
    modulePistas(response.pistas);
};

/*
 *  MODULO PISTA
 *  Aquesta funció activa el módul de pistes.
 * 
 */

function modulePistas(pistas){
    var i = 0,
        total = pistas.length;

    var btn = document.querySelector('.o-game-module .a-button--pistes'),
        count = btn.querySelector('.total'),
        cont = document.querySelector('.o-game-module .o-pista-container'),
        reset = document.querySelector('.o-game-module .a-button--answer');

    btn.addEventListener('click',addPista,false);
    cont.addEventListener('click',function(event){removePista(event)},false);
    reset.addEventListener('click',resetPista,false);


    function addPista(){
        if(i<total){
            var ht = '<article id="m-pista-item-'+i+'" class="m-pista-item">';
            ht += '<span class="close"></span>'+pistas[i]+'</article>';
            i += 1;
            cont.innerHTML += ht;
            count.innerHTML = "("+i+")";
        }
    }
    
    function removePista(event){
        if (!event.target.matches('.close')) return
        var el = event.target.jsClosest('.m-pista-item'),
            cont = el.jsClosest('.o-pista-container');

        cont.removeChild(el);
    }

    function resetPista(){
        i = 0;
        cont.innerHTML = "";
        count.innerHTML = "";
    }
}

/*
 *  MODULO RESPOSTES
 *  Aquesta funció analitza el response i llança la pregunta adecuada
 * 
 */

function moduleAnswers(response){
    var type = response.games;

    switch(type){
        case "chooseText":
            layoutChoose(response);      
            answerChoose(response);    
            break;
        case "chooseImg":
            layoutChooseImg(response);      
            answerChooseImg(response);    
            break;
        case "multiChooseImg":
            layoutMultiChooseImg(response);
            answerMultiChooseImg(response);
            break;
        case "latlong":
            layoutLatLong(response);   
            answerLatLong(response);
            break;
        case "orderImg":
            layoutOrderImg(response);      
            answerOrderImg(response);    
            break;
        case "puzzle":
            layoutPuzzle(response);   
            answerPuzzle(response);
            break;
        case "puzzleText":
            layoutPuzzleText(response);
            answerPuzzleText(response);    
            break;
        case "rating":
            layoutRating(response);    
            answerRating(response);
            break;
        case "text":
            layoutString(response);    
            answerString(response);
            break;
    }
}

/*
 * FUNCTION LAYOUT SUCCESS
 * Aquesta funció munta l'escrutra de la pagina success  
 * 
 */
(function(){
    var el = document.querySelector('.p-game-page--success');


    if(el!==null){
        var store = getStorage();
            response = store.currentResponse;

        document.querySelector('.oh-steps .current').innerText = response.step;
        document.querySelector('.oh-steps .total').innerText = response.totalSteps;
        document.querySelector('.oh-title').innerText = response.place;
        document.querySelector('.msi-interest p').innerText = response.extra;
        document.getElementById('action-button').addEventListener('click',function(){action();},false)
    }

    function action(){
        window.location = base_url+'questions/question.html';
    };
})();


/*
 * FUNCTION LAYOUT GENERAL DE PREGUNTA
 * Aquesta funció munta l'estructura general de la pagina pregunta.
 *
 */
function layoutQuestion(response){
    document.querySelector('.oh-steps .current').innerText = response.step;
    document.querySelector('.oh-steps .total').innerText = response.totalSteps;
    document.querySelector('.oh-title').innerText = response.place;
    document.querySelector('.ogm-header .content p').innerText = response.story;
    document.querySelector('.ogm-question .question').innerText = response.question;
    document.querySelector('.ogm-header').style.backgroundImage = "url('"+response.coverImg+"')";   
}

/*
 * FUNCTION LAYOUT CHOOSE IMG
 * Aquesta funció munta l'estructura d'escollir una única imatge.
 *
 */
function layoutChoose(response){
    layoutQuestion(response);
    var contentDiv = '<div class="m-question-item step1">';
    var names = response.options,
        total = names.length,
        randomArr = randomArray(total);
        for(var i = 0; i<total; i++){
            contentDiv += '<label class="a-choose-text" for="i'+names[randomArr[i]]+'">'
            contentDiv += '<input type="radio" name="iChoose" id="i'+names[randomArr[i]]+'" value="'+names[randomArr[i]]+'" />';
            contentDiv += '<div class="act-check"><span></span></div>';
            contentDiv += '<div>'+names[randomArr[i]]+'</div>';
            contentDiv += '</label>';
        }
        contentDiv += '</div>';
        contentDiv += '<div class="m-question-item qbutton-wrap">';
        contentDiv += '<button type="button" id="action-button" class="a-button--primary">'+lang.verificarRespuesta+'</button>';
        contentDiv += '</div>';
    document.querySelector('.o-questions-container').innerHTML = contentDiv;
}

/*
 * FUNCTION LAYOUT MULTI CHOOSE IMG
 * Aquesta funció munta l'estructura d'escollir multimatge.
 *
 */
function layoutChooseImg(response){
    layoutQuestion(response);
    var contentDiv = '<div class="m-question-item step1">';
    var img = response.optionsImg[0],
        names = response.options[0],
        total = img.length,
        randomArr = randomArray(total);
        for(var i = 0; i<total; i++){
            contentDiv += '<label class="a-choose-img" for="i'+names[randomArr[i]]+'">'
            contentDiv += '<input type="radio" name="iChoose" id="i'+names[randomArr[i]]+'" value="'+names[randomArr[i]]+'" />';
            contentDiv += '<span><img src="'+img[randomArr[i]]+'" class="w100" /></span>';
            contentDiv += '</label>';
        }
        contentDiv += '</div>';
        contentDiv += '<div class="m-question-item qbutton-wrap">';
        contentDiv += '<button type="button" id="action-button" class="a-button--primary">'+lang.verificarRespuesta+'</button>';
        contentDiv += '</div>';
    document.querySelector('.o-questions-container').innerHTML = contentDiv;
}

/*
 * FUNCTION LAYOUT LAT LONG
 * Aquesta funció munta l'estructura de geoposicionament.
 *
 */
function layoutLatLong(response){
    layoutQuestion(response);
    var contentDiv =  '<p id="message"></p>';
        contentDiv += '<div class="m-question-item qbutton-wrap">';
        contentDiv += '<button type="button" id="action-button" class="a-button--primary">'+lang.verificarPosicion+'</button>';
        contentDiv += '</div>';
    document.querySelector('.o-questions-container').innerHTML = contentDiv;
}

/*
 * FUNCTION LAYOUT MULTI CHOOSE IMG
 * Aquesta funció munta l'estructura d'escollir multimatge.
 *
 */
function layoutMultiChooseImg(response){
    layoutQuestion(response);
    var contentDiv = '<div class="m-question-item step1">';
    var img = response.optionsImg[0],
        names = response.options[0],
        total = img.length,
        randomArr = randomArray(total);

        for(var i = 0; i<total; i++){
            contentDiv += '<label class="a-choose-img" for="i'+names[randomArr[i]]+'">'
            contentDiv += '<input type="checkbox" name="iChoose" id="i'+names[randomArr[i]]+'" value="'+names[randomArr[i]]+'" />';
            contentDiv += '<span><img src="'+img[randomArr[i]]+'" class="w100" /></span>';
            contentDiv += '</label>';
        }
        contentDiv += '</div>';
        contentDiv += '<div class="m-question-item qbutton-wrap">';
        contentDiv += '<button type="button" id="action-button" class="a-button--primary">'+lang.verificarRespuesta+'</button>';
        contentDiv += '</div>';
    document.querySelector('.o-questions-container').innerHTML = contentDiv;
}

/*
 * FUNCTION LAYOUT ORDER IMG
 * Aquesta funció munta l'estructura d'ordenació d'imatges.
 *
 */
function layoutOrderImg(response){
    layoutQuestion(response);
    var img = response.optionsImg[0],
    names = response.options,
    total = img.length,
    randomArr = randomArray(total);

    var contentDiv = '<div class="m-question-item step1">';
        contentDiv += '<div class="drag-and-drop-item">';
        for(var i = 0; i<total; i++){
            contentDiv += '<div class="a-choose-img" data-name="'+names[randomArr[i]]+'">';
            contentDiv += '<span><img src="'+img[randomArr[i]]+'" class="w100" /></span></div>';
        }
        contentDiv += '</div></div>';
        contentDiv += '<div class="m-question-item qbutton-wrap">';
        contentDiv += '<button type="button" id="action-button" class="a-button--primary">'+lang.verificarRespuesta+'</button>';
        contentDiv += '</div>';
    document.querySelector('.o-questions-container').innerHTML = contentDiv;
}



/*
 * FUNCTION LAYOUT PUZZLE
 * Aquesta funció munta l'estructura de la proba de puzzle
 *
 */
function layoutPuzzle(response){
    layoutQuestion(response);
    var contentDiv = '<div class="m-question-item step1">';
        contentDiv += '<div id="puzzle-containment">';
        contentDiv += '<div>';
        contentDiv += '<img id="source_image" class="w100" src="">';
        contentDiv += '</div><div id="pile"></div></div></div>';
        contentDiv += '<div class="m-question-item qbutton-wrap">';
        contentDiv += '<button type="button" id="action-button" class="a-button--primary">'+lang.verificarRespuesta+'</button>';
        contentDiv += '</div>';
    document.querySelector('.o-questions-container').innerHTML = contentDiv;
}

/*
 * FUNCTION LAYOUT PUZZLE TEXT
 * Aquesta funció munta l'estructura de la proba doble de puzzle i string
 *
 */
function layoutPuzzleText(response){
    layoutQuestion(response);
    var contentDiv = '<div class="m-question-item step1">';
        contentDiv += '<div id="puzzle-containment">';
        contentDiv += '<div>';
        contentDiv += '<img id="source_image" class="w100" src="">';
        contentDiv += '</div><div id="pile"></div></div></div>';
        contentDiv += '<div class="m-question-item step2">';
        contentDiv += '<input class="boxed-input" type="text" name="iquestion" id="iquestion" placeholder="'+lang.introduceRespuesta+'">';
        contentDiv += '</div>';
        contentDiv += '<div class="m-question-item qbutton-wrap">';
        contentDiv += '<button type="button" id="action-button" class="a-button--primary">'+lang.verificarRespuesta+'</button>';
        contentDiv += '</div>';
    document.querySelector('.o-questions-container').innerHTML = contentDiv;
}

/*
 * FUNCTION LAYOUT RATING
 * Aquesta funció munta l'estructura de la proba derating
 *
 */
function layoutRating(response){
    layoutQuestion(response);
    var contentDiv = '<div class="m-question-item step1">';
        contentDiv += '<div class="rating-game"><input type="text" class="rating"></div>';
        contentDiv += '</div>';
        contentDiv += '<div class="m-question-item qbutton-wrap">';
        contentDiv += '<button type="button" id="action-button" class="a-button--primary">'+lang.verificarRespuesta+'</button>';
        contentDiv += '</div>';
    document.querySelector('.o-questions-container').innerHTML = contentDiv;
}

/*
 * FUNCTION LAYOUT TEXT
 * Aquesta funció munta l'estructura de la proba doble de puzzle i string
 *
 */
function layoutString(response){
    layoutQuestion(response);
    var contentDiv = '<div class="m-question-item step1">';
        contentDiv += '<input class="boxed-input" type="text" name="iquestion" id="iquestion" placeholder="'+lang.introduceRespuesta+'">';
        contentDiv += '</div>';
        contentDiv += '<div class="m-question-item qbutton-wrap">';
        contentDiv += '<button type="button" id="action-button" class="a-button--primary">'+lang.verificarRespuesta+'</button>';
        contentDiv += '</div>';
    document.querySelector('.o-questions-container').innerHTML = contentDiv;
}

/*
 *  FUNCTION ANSWER CHOOSE
 *  Aquesta funció llança la funcionalitat de les preguntes tipus choose de tipus radi.
 * 
 */

function answerChoose(response){
    var ok = response.answers[0],
        btn = document.getElementById('action-button');
    
    btn.addEventListener('click', function(){checkAnswer();},false);

    function checkAnswer(){
        var el = document.querySelector('input[name="iChoose"]:checked');

        if(el.value === ok){
            nextSuccess(response);
        }else{
            openModal('game-error');
        }
    }
}

/*
 *  FUNCTION ANSWER CHOOSE
 *  Aquesta funció llança la funcionalitat de les preguntes tipus choose de tipus radi.
 * 
 */

function answerChooseImg(response){
    var ok = response.answers[0][0],
        btn = document.getElementById('action-button');
    
    btn.addEventListener('click', function(){checkAnswer();},false);

    function checkAnswer(){
        var el = document.querySelector('input[name="iChoose"]:checked');
        if(el.value === ok){
            nextSuccess(response);
        }else{
            openModal('game-error');
        }
    }
}

/*
 * FUNCTION ANSWER LATLONG
 * Aquesta funció llança la funcionalitat de la comprobació de geoposicionament.
 *
 */
function answerLatLong(response){
    var btn = document.getElementById('action-button'),
        x = document.getElementById('message');
    
    btn.addEventListener('click', function(){checkAnswer();},false);

    function checkAnswer(){
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(action,showErrors);
        } else {
            x.innerHTML = lang.GeolocationNotSupported;
            openModal('game-error');
        }        
        
        function action(position){
            var latitude = parseFloat(response.answers[0].lat),
                longitude = parseFloat(response.answers[0].long),
                check = getDistanceFromLatLonInMeters(position.coords.latitude, position.coords.longitude, latitude, longitude).toFixed(1);

            if(check<=350){
                nextSuccess(response);
            }else{
                x.innerHTML = lang.GeolocationIsTooFar;
                openModal('game-error');
            }
        }

        function showErrors(error){
            switch(error.code){
                case error.PERMISSION_DENIED:
                x.innerHTML = lang.GeolocationUserDenied;
                openModal('game-error');
                break;
                case error.POSITION_UNAVAILABLE:
                x.innerHTML = lang.GeolocationInfoUnavailable;
                openModal('game-error');
                break;
                case error.TIMEOUT:
                x.innerHTML = lang.GeolocationTimedOut;
                openModal('game-error');
                break;
                case error.UNKNOWN_ERROR:
                x.innerHTML = lang.GeolocationUnknownError;
                openModal('game-error');
                break;
            }
        }
    }
}

/*
 * FUNCTION ANSWER MULTI CHOOSE IMG
 * Aquesta funció llança la funcionalitat de les preguntes tipus opcio multiple d'imatge.
 *
 */
function answerMultiChooseImg(response){
    var ok = response.answers[0],
        el = document.getElementsByName('iChoose'),
        btn = document.getElementById('action-button');
    
    btn.addEventListener('click', function(){checkAnswer();},false);

    function checkAnswer(){
        
        var goodies = 0,
            total = ok.length,
            aux = true;

        el.forEach(function(usEl){
            if(usEl.checked && ok.indexOf(usEl.value) !== -1){
                goodies += 1;
            }else if(usEl.checked){
                aux = false;
            }
        });

        if(goodies === total && aux){
            nextSuccess(response);
        }else{
            openModal('game-error');
        }
    }
}

/*
 * FUNCTION ANSWER ORDER
 * Aquesta funció llança la funcionalitat de ordenar una secuencia d'imatge amb drag and drop
 *
 */
function answerOrderImg(response){
    var ok = response.answers,
        btn = document.getElementById('action-button');


        $(function() {
            $( ".drag-and-drop-item" ).sortable();
            $( ".drag-and-drop-item" ).disableSelection();
        });

        btn.addEventListener('click', function(){checkAnswer();}, false);

        function checkAnswer(){
            var auxArr = [];
            $('.a-choose-img').each(function(){
                auxArr.push($(this).data('name'));
            });
            if(compareTwoArrays(auxArr, ok)){
                nextSuccess(response);
            }else{
                openModal('game-error');
            }
        }
}

/*
 *  FUNCTION ANSWER PUZZLE
 *  Aquesta funció llança la funcionalitat de les preguntes tipus puzzle
 * 
 */

function answerPuzzle(response){
    var url = response.optionsImg[0],
        i = 0,
        aux = false,
        btn = document.getElementById('action-button');

        document.querySelector('.o-game-module .a-button--answer').addEventListener('click', function(){
            setTimeout(function(){
                action()
            },100);
        }, false);

        function action(){
            if(i === 0){
                $('#source_image').attr('src',url);
                $('#pile').height($('#source_image').height());

                start_puzzle(2);
            }
            i++;

            $(window).resize(function(){
                if(!$('#pile').hasClass('success')){
                    $('#pile').height($('#source_image').height());
                }
                $('#source_image').snapPuzzle('refresh');
            });
        }

        function start_puzzle(x){
            $('#source_image').snapPuzzle({
                rows: x, columns: x,
                pile: '#pile',
                containment: '#puzzle-containment',
                onComplete: function(){
                    $('#pile').height('0px');
                    $('#pile').addClass('success');
                    aux = true;
                }
            });
        }

        
        btn.addEventListener('click', function(){checkAnswer();},false);

        function checkAnswer(){
            if(aux){
                nextSuccess(response);
            }else{
                openModal('game-error');
            }
        }
}

/*
 *  FUNCTION ANSWER PUZZLE
 *  Aquesta funció llança la funcionalitat de les preguntes tipus puzzle
 * 
 */

function answerPuzzleText(response){
    var url = response.optionsImg,
        i = 0, 
        ok = response.answers[1],
        el = document.getElementById('iquestion'),
        btn = document.getElementById('action-button');

        document.querySelector('.o-game-module .a-button--answer').addEventListener('click', function(){
            setTimeout(function(){
                action()
            },50);
        }, false);

        function action(){
            if(i === 0){
                $('#source_image').attr('src',url);
                if(!$('#pile').hasClass('success')){
                    $('#pile').height($('#source_image').height());
                }
                start_puzzle(2);
            }
            i++;

            $(window).resize(function(){
                if(!$('#pile').hasClass('success')){
                    $('#pile').height($('#source_image').height());
                }
                $('#source_image').snapPuzzle('refresh');
            });
        }

        function start_puzzle(x){
            $('#source_image').snapPuzzle({
                rows: x, columns: x,
                pile: '#pile',
                containment: '#puzzle-containment',
                onComplete: function(){
                    $('#pile').height('0px');
                    $('#pile').addClass('success');
                }
            });
        }

        btn.addEventListener('click', function(){checkAnswer();},false);

        function checkAnswer(){
            if(compareArrayStrings(el.value,ok)){
                nextSuccess(response);
            }else{
                openModal('game-error');
            }
        }
}

/*
 *  FUNCTION ANSWER RATING
 *  Aquesta funció llança la funcionalitat de les preguntes tipus choose (escollir entre varies imatges).
 * 
 */

function answerRating(response){
    var ok = response.answers[0],
        el = document.querySelector('.rating-game .rating'),
        btn = document.getElementById('action-button');

    $(".rating").rating({
        bgUnrated: response.optionsImg.unrated,
        bgRated: response.optionsImg.rated,
        stars: response.options[0],
        size: 260/response.options[0],
    });


    btn.addEventListener('click', function(){checkAnswer();},false);

    function checkAnswer(){
        if(compareNumber(el.value,ok)){
            nextSuccess(response);
        }else{
            openModal('game-error');
        }
    }   
}

/*
 *  FUNCTION ANSWER STRING
 *  Aquesta funció llança la funcionalitat de les preguntes tipus string
 * 
 */

function answerString(response){
    var ok = response.answers[0],
        el = document.getElementById('iquestion'),
        btn = document.getElementById('action-button');

    btn.addEventListener('click', function(){checkAnswer();},false);

    function checkAnswer(){
        if(compareArrayStrings(el.value,ok)){
            nextSuccess(response);
        }else{
            openModal('game-error');
        }
    }
}


/*
 *  NEXT SUCCESS
 *  Aquesta funció pasa a la seguent pantalla
 * 
 */
function nextSuccess(response){
    
    if(response.nextStep !== -1){
        var store = getStorage();
            store.currentStep = response.nextStep;
            store.currentResponse = response;
            
            setStorage(store);

        window.location = base_url+'questions/success.html';
    }else{
        var store = getStorage();
            delete store.currentStep;
            store.currentResponse = response;
        setStorage(store);

        window.location = base_url+'questions/finpartida.html';
    }
}

(function(){

    var inp = document.querySelector('#myFileInput'),
        el = document.querySelector('#myFileUpload');

    if(inp!==null && el!==null){

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                el.innerHTML = "<img src='' id='myFileImg' class='w100' />"

                var elImg = document.querySelector('#myFileImg');

                reader.onload = function (e) {
                    elImg.setAttribute('src', e.target.result);
                }
        
                reader.readAsDataURL(input.files[0]);
            }
        }

        // function sendPic() {
        //     var file = inp.files[0];
        //     console.log(file);
        // }
        
        inp.addEventListener('change', function(){ readURL(this); }, false);              
        el.addEventListener('click', function(e) { inp.click(); }, false);

    }
})();

(function(){
    var el = document.querySelector('.p-init-register-form');

    if(el !== null){
        var btn1 = document.getElementById('bFirstStep'),
            btn2 = document.getElementById('bSecondStep'),
            btn3 = document.getElementById('bFinalStep'),
            name = document.getElementById('iName'),
            email = document.getElementById('iEmail'),
            pass1 = document.getElementById('iPassword'),
            pass2 = document.getElementById('iRepassword'),
            birthday = document.getElementById('iBirthday'),
            country = document.getElementById('iCountry'),
            city = document.getElementById('iCity');

        btn1.addEventListener('click', function(){ checkForm1(); }, false);
        btn2.addEventListener('click', function(){ checkForm2(); }, false);
        btn3.addEventListener('click', function(){ checkForm3(); }, false);
    }

    function checkForm1(){

        if(name.value !== "" && email.value !== "" && pass1.value !== "" && pass2.value !== "" && pass1.value == pass2.value && birthday.value !== ""){
            $('.step.step1').removeClass('active');
            $('.step.step2').addClass('active');
        }
    };

    function checkForm2(){
        if(country.value !== "" && city.value !== ""){
            openModal('politica');
        }
    };

    function checkForm3(){
        window.location = base_url;
    };

})();

(function(){
    $('.icon-modal').on('click', function(){
        var type = $(this).data('type');
        openModal(type);           
    });
})();

function openModal(type){
    $('.m-modal').each(function(){
        if($(this).hasClass(type)){
            $(this).addClass('open');
            
            $('.modal-close').on('click', function(){
                closeModal($(this));
            })
        }
    })
}

function closeModal($close){
    $close.closest('.m-modal').removeClass('open');
}