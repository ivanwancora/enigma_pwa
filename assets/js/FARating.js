/**
 * Versatile star rating plugin
 * Version 1.3
 * Made by Dominik Vávra
 */

 (function ( $ ) {

     $.fn.rating = function( options ) {

       // Default settings
       var defaults = $.extend({
           bgUnrated: "assets/img/star_unrated.png",
           bgRated: "assets/img/star.png",
           stars: 5,
           size: 100,
           margin: 1,
           startAt: false,
           callback: false,
           readonly: false
       }, options );

        // Getting data attributes
        var data_bgunrated = this.data("rating-bgunrated");
        var data_bgrated = this.data("rating-bgrated");
        var data_stars = this.data("rating-stars");
        var data_size = this.data("rating-size");
        var data_margin = this.data("rating-margin");
        var data_callback = this.data("rating-callback");
        var data_startAt = this.data("rating-startat");
        var data_readonly = this.data("rating-readonly");

        // The data attributes rewrite default options.

        if(data_bgunrated) {
          defaults.bgUnrated = data_bgunrated;
        }
        if(data_bgrated) {
          defaults.bgRated = data_bgrated;
        }
        if(data_stars) {
          defaults.stars = data_stars;
        }
        if(data_size) {
          defaults.size = data_size;
        }
        if(data_margin) {
          defaults.margin = data_margin;
        }
        if(data_callback) {
          defaults.callback = data_callback;
        }
        if(data_startAt) {
          defaults.startAt = data_startAt;
        }
        if(data_readonly) {
          defaults.readonly = data_readonly;
        }

        // As first step we have to hide original input but define variable with it.
        this.hide();
        var input = this;


        function setRating(holder) {
          holder.find(".unrated").css({
            backgroundImage : "url('"+defaults.bgUnrated+"')",
            height: defaults.size,
            width: defaults.size
          });

          holder.find(".rated").css({
            backgroundImage : "url('"+defaults.bgRated+"')",
            height: defaults.size,
            width: defaults.size
          });
        }

        // Next one we will create holder for our stars.
        this.after('<div class="stars_holder"></div>');

        var holder = this.next();
        if(defaults.readonly == true) {
          holder.addClass("readonly-holder");
        }

        // Then we will for loop our stars count and add stars into holder.
        if(defaults.stars > 0) {
          for (var i = 0; i < defaults.stars; i++) {
            var defaultClass = "unrated";
            if(defaults.startAt) {
              if(defaults.startAt > i) {
                defaultClass = "rated";
              }
            }
            holder.append('<span class="star '+defaultClass+'" aria-hidden="true"></span>');
            setRating(holder);
          }
        } else {
          holder.css({
            "color" : "#333"
          });
          holder.text("U should set stars parametr to 1 atleast.");
        }

        // this variable holds all stars.
        var stars = holder.find("span");
        setRating(holder);

        // add some styles to stars
        stars.css({
          "margin-right" : defaults.margin
        });

        var clicked = false;

        stars.on( "mouseover", function(star) {
          clicked = false;
          if(!clicked) {
            $(this).removeClass("unrated").addClass("rated");
            $(this).prevAll().removeClass("unrated").addClass("rated");
            $(this).nextAll().removeClass("rated").addClass("unrated");
            setRating(holder);
          }
        });

        holder.on( "mouseleave", function(star) {
          if(!clicked) {
            stars.removeClass("rated").addClass("unrated");
            setRating(holder);
            var countStars = $(input).val()
            if(countStars > 0) {
              holder.find('span:lt('+countStars+')').removeClass("unrated").addClass("rated");
            }
            setRating(holder);
          }
        });

        stars.on( "click", function(star) {
          clicked = true;
          $(this).addClass("clickedStar");
          var rating = holder.find(".rated").length;
          input.attr("value",rating);
          if(defaults.callback) {
            $(callback).html(rating);
          }
        });

        $(".clickedStar").on( "mouseleave", function(star) {
          clicked = false;
        });

        return this;
     };

 }( jQuery ));
